<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
    use HasFactory;
    //Protects model from mass assignmen. Note: Only the fields inputted in the $fillable are fillable.
    protected $fillable = [
        'title',
        'completed'
    ];
}
