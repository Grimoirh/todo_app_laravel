<?php

namespace App\Http\Controllers;

use App\Models\Todo;
use Illuminate\Http\Request;

use function GuzzleHttp\Promise\all;

class TodoController extends Controller
{
    //function for index
    public function index()
    {
        $todos = Todo::orderBy('completed')->get();
        return view('todo.index')->with(['todos' => $todos]);
    }

    //function for create
    // public function create()
    // {
    //     return view('todo.index');
    // }
    
    //function for upload
    public function upload(Request $request)
    {
        //pop a error message if the title field is empty or exceeded 255 characters
        $request->validate([
            'title' => 'required|max:255',
        ]);
        $todo = $request->title;
        Todo::create(['title' => $todo]);
        return redirect()->back();
    }
    //function for edit
    public function edit($id)
    {
        $todo = Todo::find($id);
        return view('todo.edit')->with(['id' => $id, 'todo' => $todo]);
    }
    //function for update
    public function update(Request $request)
    {
        //pop a error message if the title field is empty or exceeded 255 characters
        $request->validate([
            'title' => 'required|max:255',
        ]);
        $updateTodo = Todo::find($request->id);
        $updateTodo->update(['title' => $request->title]);
        return redirect('/');
    }
    //function for mark task as done
    public function completed($id)
    {
        $todo = Todo::find($id);
        if ($todo->completed) {
            $todo->update(['completed' => false]);
            return redirect()->back();
        } else {
            $todo->update(['completed' => true]);
            return redirect()->back();
        }
    }
    // function for mark all as done
    // This function is not used, because I don't know how to implement in the web, tried in line 40. It says $todo is undefined because there is no source above it
    public function completeAll(){
        $todo = Todo::query();
        if ($todo->completed){
            $todo->where('completed' , '=', 0)->update(['completed' => 1]);
        }else{
            $todo->where('completed' , '=', 1)->update(['completed' => 0]);
        }
    }
    //function for deleting 1 task
    public function delete($id)
    {
        $todo = Todo::find($id);
        $todo->delete();
        return redirect()->back();
    }
    //function for deleting all task
    public function deleteAll()
    {
        Todo::truncate('todos');
        return redirect()->back();
    }
}
