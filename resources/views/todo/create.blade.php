<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>To-Do List | Create</title>
</head>
<body style="text-align: center">

    <h1>Create a New To-Do</h1>

    <h3> <x-alert/> </h3>
    
    <form action="/upload" method="post">
        @csrf
        <input type="text" name="title"/>
        <input type="submit" value="Submit"/>
    </form>
    <br>
    <a href="/index">Go Back</a>
    
</body>
</html>