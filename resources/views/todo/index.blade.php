<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    {{-- css link --}}
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    {{-- font awesome script --}}
    <script src="https://kit.fontawesome.com/2fc1fce0fb.js" crossorigin="anonymous"></script>

    <title>To-Do List</title>
</head>
<body>
    <div class="main-container">
        <h1>Welcome to your To-Do List</h1>

        {{-- create or add a new in todo list --}}
        <div class="create-container">
            <form action="/upload" method="post">
                @csrf
                <input type="text" placeholder="Enter a task..." id="new" name="title"/>
                <input type="submit" onclick="return createMsg();" class="add-btn" value="Add New">
            </form>
        </div>
        
        <h3> <x-alert/> </h3>

        {{-- listing down the todo list --}}
            <div class="todo-container">
                <table>
                    @foreach($todos as $todo)
                    <tr>
                        <td>
                            {{-- draw a line-through in the finished to-do --}}
                            <div class="todo-title">
                                @if($todo->completed)
                                    <span style="text-decoration: line-through">{{$todo->title}}</span>
                                @else
                                    {{$todo->title}}
                                @endif
                            </div>
                        </td>
                        <td>
                            <div class="todo-buttons">
                                {{-- click to mark done or undone with the to-do --}}
                                @if($todo->completed)
                                    <a style="margin-right: 2px;" onclick="undoneMsg()" href="{{asset('/' . $todo->id . '/completed')}}" class="btn-link"><i class="fa-solid fa-x"></i></a>
                                @else
                                    <a style="font-size: 18px" onclick="doneMsg()" href="{{asset('/' . $todo->id . '/completed')}}" class="btn-link"><i class="fa-solid fa-check"></i></a>
                                @endif    
                                {{-- edit button --}}
                                <a href="{{asset('/' . $todo->id . '/edit')}}" class="btn-link editbtn"><i class="fa-regular fa-pen-to-square"></i></a>
                                {{-- delete button --}}
                                <a onclick="deleteConfirm()" href="{{asset('/' . $todo->id . '/delete')}}" class="btn-link"><i class="fa-solid fa-trash-can"></i></a>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
            <div class="all-btn">
                <table>
                    <tr class="btn-all">
                        <td class="td-del-all">    
                            <div class="del-all-btn">
                                <a style="font-weight: bold; color: rgb(255, 100, 3);" onclick="return deleteAllConfirm();" href="{{asset('/deleteAll')}}" class="btn-link">Delete All</a>
                            </div>
                        </td>
                        {{-- <td class="td-done-all">
                            <div class="done-all-btn">
                                @if($todo->completed)
                                <a style="font-weight: bold; color: rgb(255, 100, 3);" href="{{asset('/completeAll')}}" class="btn-link">Mark All as Undone</a>
                                @else
                                <a style="font-weight: bold; color: rgb(255, 100, 3);" href="{{asset('/completeAll')}}" class="btn-link">Mark All as Done</a>
                                @endif
                            </div>
                        </td> --}}
                    </tr>
                </table>
            </div>
    </div>
    {{-- scripts --}}
    <script>
        function createMsg(){
            if (document.getElementById('new').value == "" || null){
                alert("You must input a task in this field!");
                return false;
            }else{
                alert("Task Added to To-Do List Successfully!")
                return true;
            }
        }
        function doneMsg(){
            alert("Task Marked as Done!")
        }
        function undoneMsg(){
            alert("Task Marked as Not yet Done!")
        }
        function deleteConfirm(){
            var result = confirm("Are you sure you want to delete this task?");
            if (result==false){
                event.preventDefault();
            }else{
                alert("Task Deleted Successfully!")
            }
        }
        function deleteAllConfirm(){
            var result = confirm("Are you sure you want to delete all tasks?");
            if (result==false){
                event.preventDefault();
            }else{
                alert("Task Deleted Successfully!")
            }
        }

    </script>
    
</body>
</html>