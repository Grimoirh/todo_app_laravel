<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <script src="https://kit.fontawesome.com/2fc1fce0fb.js" crossorigin="anonymous"></script>

    <title>To-Do List | Edit</title>
</head>
<body>

<div class="main-container">
    <div class="back-btn">
        <a href="/" class="btn-back"><i class="fa-solid fa-circle-left"></i></a>
    </div>
    
    <h1>Edit your To-Do</h1>

{{-- edit the clicked task --}}
    <div class="create-container">
        <form action="/update" method="post">
            @csrf
            @method('patch')
            <input type="text" id="new" name="title" value="{{$todo->title}}">
            <input style="display: none" type="number" name="id" value="{{$todo->id}}">
            <input type="submit" onclick="return updateMsg();" class="add-btn" value="Edit Task">
        </form>
    </div>

    <h3> <x-alert/> </h3>

    </div>
    
    <script>
        function updateMsg(){
            if (document.getElementById('new').value == "" || null){
                alert("You must input a task in this field!");
                return false;
            }else{
                alert("Task Edited Successfully!")
                return true;
            }
        }

    </script>
    
</body>
</html>